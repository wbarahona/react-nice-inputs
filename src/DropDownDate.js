import React, { Component } from 'react';
import m from 'moment';
import propTypes from 'prop-types';
import utils from './utils';

// Components
import FormGroup from './FormGroup';
import Feedback from './Feedback';
import Label from './Label';

class DropDownDate extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
  }

  state = {
    classList: '',
    dayOptions: [],
    monthOptions: [],
    yearOptions: [],
    date: '',
    m: null,
    month: null,
    day: null,
    year: null
  }

  componentDidMount() {
    this.prepareClassList();
    this.prepareMonthOptions(0, 11);
    this.prepareYearOptions();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
      this.prepareClassList();
    }
    if (prevProps.minDate !== this.props.minDate || prevProps.maxDate !== this.props.maxDate) {
      this.prepareYearOptions();
    }
    if (prevProps.mmmm !== this.props.mmmm) {
      this.prepareMonthOptions(0, 11);
    }
    if (this.state.date !== null && prevState.date !== this.state.date) {
      this.handleChange(this.thisInput);
    }
    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if ((prevProps.value !== this.props.value || this.props.value !== this.state.date) && this.state.date === '') {
        this.prepopulateInput();
      }
    }
  }

  defaults = {
    minDate: '01-01-1970',
    maxDate: `12-31-${ m().year() }`,
    format: 'MM-DD-YYYY'
  }

  prepopulateInput = () => {
    const { value } = { ... this.props };
    const { format: propsFormat, minDate: propsMinDate, maxDate: propsMaxDate } = this.props;
    const { format: defaultFormat, minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;
    const format = propsFormat || defaultFormat;
    const minDate = propsMinDate || defaultMinDate;
    const maxDate = propsMaxDate || defaultMaxDate;
    let _mValDate = m(value, format, true);
    let _mMinDate = m(minDate, format, true);
    let _mMaxDate = m(maxDate, format, true);

    if (value) {
      // if _mValDate is invalid then try rebuild it with default format
      if (!_mValDate.isValid()) {
        _mValDate = m(value, defaultFormat, true);
      }
      // if _mMinDate is invalid then try rebuild it with default format
      if (!_mMinDate.isValid()) {
        _mMinDate = m(minDate, defaultFormat, true);
      }
      // if _mMaxDate is invalid then try rebuild it with default format
      if (!_mMaxDate.isValid()) {
        _mMaxDate = m(maxDate, defaultFormat, true);
      }

      // validate if value is a valid date format vs the format prop
      // validate against min dates and max dates to check if its within the selectable ranges

      if (_mValDate.isValid() && _mValDate.isBetween(_mMinDate, _mMaxDate)) {
        const daysInMonth =  _mValDate.daysInMonth();

        this.prepareDayOptions(1, daysInMonth);
        this.setState({ date: value, m: _mValDate, month: _mValDate.month() + 1, day: _mValDate.date(), year: _mValDate.year() });
      } else {
        console.warn(`'react-nice-inputs': The value prop is not matching the format: "${ format }" or is not a valid date value or is out of min/max dates range.`);
      }
    }
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid, mmClassList, ddClassList, yyyyClassList } = this.props;
    const mmClassListString = utils.createClassList(mmClassList);
    const ddClassListString = utils.createClassList(ddClassList);
    const yyyyClassListString = utils.createClassList(yyyyClassList);
    const classListString = utils.createClassList(classList, isValid, isInvalid);

    this.setState({ classList: classListString, mmClassList: mmClassListString, ddClassList: ddClassListString, yyyyClassList: yyyyClassListString });
  }

  prepareMonthOptions = (firstMonth, lastMonth) => {
    const { mmmm } = this.props;
    const monthOptions = [];

    for (let i = firstMonth; i <= lastMonth; ++i) {
      const opt = { value: i + 1 };

      opt.label = (mmmm) ? m().month(i).format('MMMM') : i + 1;

      monthOptions.push(opt);
    }

    this.setState({ monthOptions });
  };

  prepareDayOptions = (firstDay, lastDay) => {
    const dayOptions = [];

    for (let i = firstDay; i <= lastDay; ++i) {
      const opt = { label: i, value: i };

      dayOptions.push(opt);
    }

    this.setState({ dayOptions });
  }

  prepareYearOptions = () => {
    const yearOptions = [];
    const { format: propsFormat, minDate: propsMinDate, maxDate: propsMaxDate } = this.props;
    const { format: defaultFormat, minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;
    const format = propsFormat || defaultFormat;
    const minDate = propsMinDate || defaultMinDate;
    const maxDate = propsMaxDate || defaultMaxDate;

    let _mMinDate = m(minDate, format, true);
    let _mMaxDate = m(maxDate, format, true);

    // if _mMinDate is invalid then try rebuild it with default format
    if (!_mMinDate.isValid()) {
      _mMinDate = m(minDate, defaultFormat, true);
    }
    // if _mMaxDate is invalid then try rebuild it with default format
    if (!_mMaxDate.isValid()) {
      _mMaxDate = m(maxDate, defaultFormat, true);
    }

    // const validDates = this.validateDates();
    const initialYear = _mMinDate.year();
    const finalYear = _mMaxDate.year();

    for (let year = finalYear; year >= initialYear; year--) {
      const opt = { label: year, value: year };

      yearOptions.push(opt);
    }

    this.setState({ yearOptions }, () => {
      this.checkDateRanges();
    });
  }

  validateDates = () => {
    const { minDate: propsMinDate, maxDate: propsMaxDate, format: propsFormat } = { ... this.props };
    const { minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;
    const response = {
      minDateValid: true,
      maxDateValid: true
    };
    let minDate = '';
    let maxDate = '';
    let format = '';

    minDate = propsMinDate || defaultMinDate;
    maxDate = propsMaxDate || defaultMaxDate;

    if (propsFormat && (propsMinDate || propsMaxDate)) {
      format = propsFormat;

      response.minDateValid = m(minDate, format, true).isValid();
      response.maxDateValid = m(maxDate, format, true).isValid();
    }

    if (!response.minDateValid && propsMinDate && propsFormat) {
      console.warn(`'react-nice-inputs': Dates format provided missmatches the format props, resorting to default dates, please send "minDate" prop following the format: ${ propsFormat }`);
    }
    if (!response.maxDateValid && propsMaxDate && propsFormat) {
      console.warn(`'react-nice-inputs': Dates format provided missmatches the format props, resorting to default dates, please send "maxDate" prop following the format: ${ propsFormat }`);
    }

    return response;
  }

  prepareFragmentedDateRanges = () =>{
    const dateRanges = {
      minDate: {
        month: null,
        day: null,
        year: null
      },
      maxDate: {
        month: null,
        day: null,
        year: null
      }
    };
    // const { minDate, maxDate, format } = this.props;
    const { format: propsFormat, minDate: propsMinDate, maxDate: propsMaxDate } = { ... this.props };
    const { format: defaultFormat, minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;
    const format = propsFormat || defaultFormat;
    const minDate = propsMinDate || defaultMinDate;
    const maxDate = propsMaxDate || defaultMaxDate;
    let _mMinDate = m(minDate, format, true);
    let _mMaxDate = m(maxDate, format, true);

    // if _mMinDate is invalid then try rebuild it with default format
    if (!_mMinDate.isValid()) {
      _mMinDate = m(minDate, defaultFormat, true);
    }
    // if _mMaxDate is invalid then try rebuild it with default format
    if (!_mMaxDate.isValid()) {
      _mMaxDate = m(maxDate, defaultFormat, true);
    }

    dateRanges.minDate.month = _mMinDate.month();
    dateRanges.minDate.day = _mMinDate.date();
    dateRanges.minDate.year = _mMinDate.year();

    dateRanges.maxDate.month = _mMaxDate.month();
    dateRanges.maxDate.day = _mMaxDate.date();
    dateRanges.maxDate.year = _mMaxDate.year();

    return dateRanges;
  };

  buildDate = (value, concept, group) => {
    const stateCopy = { ... this.state };

    stateCopy[group] = (value !== '') ? parseInt(value, 10) : null;

    this.setState({ month: stateCopy.month, day: stateCopy.day, year: stateCopy.year }, () => {

      const { format: propsFormat, minDate: propsMinDate, maxDate: propsMaxDate } = { ... this.props };
      const { format: defaultFormat, minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;

      const monthValue = stateCopy.month;
      const dayValue = stateCopy.day;
      const yearValue = stateCopy.year;

      const monthConcept = 'MM';
      const yearConcept = 'YYYY';
      const dateRanges = this.prepareFragmentedDateRanges();
      const format = propsFormat || defaultFormat;
      const minDate = propsMinDate || defaultMinDate;
      const maxDate = propsMaxDate || defaultMaxDate;
      let _mMinDate = m(minDate, format, true);
      let _mMaxDate = m(maxDate, format, true);
      let jsdate = null;
      let date = null;
      let dateFormat = '';
      let daysInMonth = 0;

      // if _mMinDate is invalid then try rebuild it with default format
      if (!_mMinDate.isValid()) {
        _mMinDate = m(minDate, defaultFormat, true);
      }
      // if _mMaxDate is invalid then try rebuild it with default format
      if (!_mMaxDate.isValid()) {
        _mMaxDate = m(maxDate, defaultFormat, true);
      }

      if (monthValue !== '') {
        date = monthValue;
        dateFormat = monthConcept;
      }
      if (monthValue !== '' && yearValue !== '' && !isNaN(yearValue)) {
        date = `${ yearValue }-${ monthValue }`;
        dateFormat = `${ yearConcept }-${ monthConcept }`;
      }

      daysInMonth =  m(date, dateFormat).daysInMonth(); // does not need true as third param

      this.prepareDayOptions(1, daysInMonth);

      // Check if years picked correspond to either max year or min year and rebuild month options
      if (dateRanges.minDate.year < yearValue && yearValue < dateRanges.maxDate.year) {
        this.prepareMonthOptions(0, 11);
      } else if (yearValue === dateRanges.minDate.year) {
        this.prepareMonthOptions(dateRanges.minDate.month, 11);
      } else if (yearValue === dateRanges.maxDate.year) {
        this.prepareMonthOptions(0, dateRanges.maxDate.month);
      }

      // Check if years picked and month picked correspond to either max year or min year and rebuild days options
      if (yearValue === dateRanges.minDate.year && monthValue - 1 === dateRanges.minDate.month) {
        this.prepareDayOptions(dateRanges.minDate.day, daysInMonth);
      } else if (yearValue === dateRanges.maxDate.year && monthValue - 1 === dateRanges.maxDate.month) {
        this.prepareDayOptions(1, dateRanges.maxDate.day);
      }

      if (monthValue !== '' && dayValue !== '' && yearValue !== '' && monthValue !== null && dayValue !== null && yearValue !== null && !isNaN(monthValue) && !isNaN(dayValue) && !isNaN(yearValue)) {
        jsdate = new Date(yearValue, monthValue - 1, dayValue);
        date = m(jsdate).format(format);
        let _mValDate = m(date, format, true);

        // if _mValDate is invalid then try rebuild it with default format
        if (!_mValDate.isValid()) {
          _mValDate = m(date, defaultFormat);
        }

        const withinDateRange = _mValDate.isBetween(_mMinDate,  _mMaxDate, false, true);

        if (withinDateRange) {
          this.setState({ date, m: m(jsdate), month: monthValue, day: dayValue, year: yearValue });
        } else  {
          this.setState({ date: '' });
        }
      } else {
        this.setState({ date: '' });
      }
    });
  }

  handleChange = () => {
    this.props.onChange(this.state.date, this.props.name, this.thisInput.current, this.state.m);
  }

  handlePick = (value, name, e) => {
    const el = e.currentTarget;
    const group = el.getAttribute('data-group');
    const concept = el.getAttribute('data-concept');

    this.buildDate(value, concept, group);
  }

  checkDateRanges = () => {
    const { format: propsFormat, minDate: propsMinDate, maxDate: propsMaxDate } = this.props;
    const { format: defaultFormat, minDate: defaultMinDate, maxDate: defaultMaxDate } = this.defaults;
    const format = propsFormat || defaultFormat;
    const minDate = propsMinDate || defaultMinDate;
    const maxDate = propsMaxDate || defaultMaxDate;
    const _mValDate = m(this.state.date, format, true);
    const _mMinDate = m(minDate, format, true);
    const _mMaxDate = m(maxDate, format, true);

    const withinDateRange = _mValDate.isBetween(_mMinDate,  _mMaxDate, false, true);

    if (!withinDateRange) {
      this.resetComponent();
    }
  }

  resetComponent = () => {
    this.prepareDayOptions(0, 31);
    this.prepareMonthOptions(0, 11);
    this.setState({
      date: '',
      month: null,
      day: null,
      year: null
    });
  }

  render() {
    const { name, label, feedback, defaultTextM, defaultTextD, defaultTextY, labelM, labelD, labelY, attrs } = this.props;
    const { date, month, day, year, classList, mmClassList, ddClassList, yyyyClassList, monthOptions, dayOptions, yearOptions } = this.state;

    return (
      <div className={ `${ classList } ddd-wrap` }>
        { label && <Label htmlFor={ name }>{ label }</Label> }
        <input ref={ this.thisInput } type="hidden" name={ name } id={ name } value={ date } { ... attrs } />
        <div className={ `${ mmClassList } month-select` }>
          <FormGroup type="select"
            name={ `mm-${ name }` }
            onChange={ this.handlePick }
            label={ labelM }
            options={ monthOptions }
            defaultText={ defaultTextM }
            attrs={ { 'data-concept': 'MM', 'data-group': 'month' } }
            value={ month }
          />
        </div>
        <div className={ `${ ddClassList } day-select` }>
          <FormGroup type="select"
            name={ `dd-${ name }` }
            onChange={ this.handlePick }
            label={ labelD }
            options={ dayOptions }
            defaultText={ defaultTextD }
            attrs={ { 'data-concept': 'DD', 'data-group': 'day' } }
            value={ day }
          />
        </div>
        <div className={ `${ yyyyClassList } year-select` }>
          <FormGroup type="select"
            name={ `yyyy-${ name }` }
            onChange={ this.handlePick }
            label={ labelY }
            options={ yearOptions }
            defaultText={ defaultTextY }
            attrs={ { 'data-concept': 'YYYY', 'data-group': 'year' } }
            value={ year }
          />
        </div>
        { feedback && <Feedback copy={ feedback } /> }
      </div>
    );
  }
}

DropDownDate.propTypes = {
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  attrs: propTypes.object,
  onChange: propTypes.func.isRequired,
  defaultTextM: propTypes.string,
  defaultTextD: propTypes.string,
  defaultTextY: propTypes.string,
  labelM: propTypes.string,
  labelD: propTypes.string,
  labelY: propTypes.string,
  label: propTypes.string,
  feedback: propTypes.node,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  mmmm: propTypes.bool,
  format: propTypes.string,
  minDate: propTypes.string,
  maxDate: propTypes.string,
  value: propTypes.string,
  mmClassList: propTypes.array,
  ddClassList: propTypes.array,
  yyyyClassList: propTypes.array
};

DropDownDate.defaultProps = {
  defaultTextM: 'Select a Month...',
  defaultTextD: 'Select a Day...',
  defaultTextY: 'Select a Year...',
  labelM: 'Month:',
  labelD: 'Day:',
  labelY: 'Year:',
  mmmm: false,
  mmClassList: [],
  ddClassList: [],
  yyyyClassList: []
};

export default DropDownDate;
