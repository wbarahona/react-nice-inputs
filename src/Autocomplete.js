import React, { Component, Fragment } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Autocomplete extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
    this.thisMask = React.createRef();
    this.thisOptions = React.createRef();
  }

  state = {
    classList: '',
    dataList: [],
    inputName: null,
    value: '',
    maskValue: '',
    optionIndex: -1,
    optionsVisible: false
  }

  componentDidMount() {
    this.prepareClassList();
    this.prepareDataList();
    this.prepareForeignEvents();
    this.prepopulateInput();
    this.registerMouseDown();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.classList && this.props.classList) {
      if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }
    }
    if (this.state.value !== null && prevState.value !== this.state.value) {
      this.handleChange(this.thisInput);
    }

    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if (prevProps.value !== this.props.value) {
        this.prepopulateInput();
      }
    }
    if (prevProps.dataList.length < this.props.dataList.length) {
      this.prepareDataList();

      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        this.prepopulateInput();
      }
    }
  }

  componentWillUnmount() {
    this.unRegisterMouseDown();
  }

  registerMouseDown = () => {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('touchstart', this.handleClickOutside);
  };

  unRegisterMouseDown = () => {
    document.removeEventListener('mousedown', this.handleClickOutside);
    document.removeEventListener('touchstart', this.handleClickOutside);
  };

  handleClickOutside = (event) => {
    const thisAutocompleteList = this.thisOptions.current;

    if (this.thisMask && !this.thisMask.current.contains(event.target) && thisAutocompleteList) {
      // check if this.state.value correspond to any value in the options prop
      if (!thisAutocompleteList.contains(event.target)) {
        const found = this.state.dataList.filter(e => e.label === this.state.maskValue)[0];

        // this.toggleAction();
        this.closeOptions();

        if (!found) {
          this.setState({ value: '', maskValue: '' }, () => {
            this.resetOptions();
            this.handleChange(this.thisInput);
          });
        }
      }
    }
  };

  resetOptions = () => {
    const dataListCopy = [ ...this.props.dataList ];

    this.setState({ dataList: dataListCopy });
  }

  prepopulateInput = () => {
    const { value, dataList } = { ... this.props };
    const option = dataList.filter(opt => opt.value === value)[0];

    if (value && option) {
      const maskValue = option.label;

      this.setState({ value, maskValue });
    } else if (value === '') {
      this.setState({ value: '', maskValue: '' });
    }
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid } = this.props;
    let classListString = utils.createClassList(classList, isValid, isInvalid);

    classListString = `input ${ classListString }`;

    this.setState({ classList: classListString });
  }

  prepareDataList = () => {
    const { dataList } = this.props;

    this.setState({ dataList });
  }

  prepareForeignEvents = () => {
    const maskInput = this.thisMask.current;

    maskInput.onkeyup = e => {
      this.navigateThroughOptions(e.keyCode);
    };
  }

  navigateThroughOptions = (keyCode) => {
    // 40 up - 38 down - 13 enter
    const thisAutocompleteList = this.thisOptions.current;

    if (thisAutocompleteList) {
      const { children: options } = thisAutocompleteList;

      if (options) {
        const optionsLen = options.length;
        let { optionIndex } = this.state;

        if (optionsLen > 0 && (keyCode === 40 || keyCode === 38)) {
          this.clearOptionSelections();

          if (keyCode === 40 && optionIndex < optionsLen - 1) {
            optionIndex++;
          } else if (keyCode === 38 && optionIndex > 0) {
            optionIndex--;
          }
          if (options[optionIndex] !== undefined) {
            options[optionIndex].classList.add('active');
          }
          this.setState({ optionIndex });
        } else if (optionsLen > 0 && (keyCode === 13 || keyCode === 9) && options[optionIndex] !== undefined) {
          this.setAutocompleteValue(options[optionIndex]);
          this.clearOptionSelections();
        }
      }
    }
  }

  clearOptionSelections = () => {
    const thisAutocompleteList = this.thisOptions.current;

    if (thisAutocompleteList) {
      const { children: options } = thisAutocompleteList;

      if(options) {
        for (const opt in options) {
          if (options.hasOwnProperty(opt)) {
            const option = options[opt];

            option.classList.remove('active');
          }
        }
      }
    }
  }

  matches = (searchString) => {
    const dataListCopy = [ ...this.props.dataList ];
    const matches = dataListCopy.filter(item => item.label.toLowerCase().includes(searchString.toLowerCase()));

    this.setState({ dataList: matches });

    return matches;
  }

  toggleAction = () => {
    this.setState({ optionsVisible: !this.state.optionsVisible });
  }

  closeOptions = () => {
    this.setState({ optionsVisible: false });
  }

  openOptions = () => {
    this.setState({ optionsVisible: true });
  }

  toggleList = e => {
    const elem = e.currentTarget;
    const name = elem.name;

    this.setState({ inputName: name }, () => {
      this.toggleAction();
    });
  }

  selectOption = e => {
    this.setAutocompleteValue(e.currentTarget);
  }

  changeMask = e => {
    const { currentTarget } = e;
    const { value } = currentTarget;

    this.setState({ maskValue: value }, () => {
      this.matches(this.state.maskValue);

      if (this.state.maskValue.length <= 0) {
        this.setState({ value: '', optionsVisible: false });
      } else {
        this.setState({ optionsVisible: true });
      }
    });
  }

  setAutocompleteValue = (el) => {
    const maskValue = el.getAttribute('data-label');
    const value = el.getAttribute('data-value');

    this.toggleAction();
    this.setState({ optionIndex: -1, maskValue, value });
  }

  handleChange = e => {
    const name = this.thisInput.current.name;
    const value = this.state.value;

    return this.props.onChange(value, name, e);
  }

  render() {
    const { attrs, name } = this.props;
    const { classList, dataList, optionsVisible } = this.state;

    return (
      <Fragment>
        <input ref={ this.thisInput } type="hidden" name={ name } id={ `hidden-autocomplete-${ name }` } value={ this.state.value } />
        <input ref={ this.thisMask } type="text" name={ name } id={ name } className={ classList } onChange={ this.changeMask } onFocus={ this.toggleList } value={ this.state.maskValue } { ...attrs } />
        { optionsVisible &&
          <ul ref={ this.thisOptions } className="autocomplete-list" id={ `dropdown-autocomplete-${ name }` }>
            {
              dataList.map((item, index) => (
                <li key={ item.value } data-value={ item.value } data-label={ item.label } data-count={ index } onClick={ this.selectOption }>{ item.label }</li>
              ))
            }
          </ul>
        }
      </Fragment>
    );
  }
}

Autocomplete.propTypes = {
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  attrs: propTypes.object,
  onChange: propTypes.func.isRequired,
  dataList: propTypes.array.isRequired,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  value: propTypes.string,
  optionTemplate: propTypes.node
};

export default Autocomplete;
