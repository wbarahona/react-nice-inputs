import React, { Component } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Label extends Component {
  state = {
    classList: ''
  }

  componentDidMount() {
    this.prepareClassList();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.hasOwnProperty('classList') && this.props.hasOwnProperty('classList')) {
      if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }
    }
  }

  prepareClassList = () => {
    const { classList } = this.props;
    const classListString = utils.createClassList(classList);

    this.setState({ classList: classListString });
  }

  render() {
    const { htmlFor, children } = this.props;
    const { classList } = this.state;

    return(
      <label htmlFor={ htmlFor } className={ `${ classList } control-label` }>{ children }</label>
    );
  }
}

Label.propTypes = {
  htmlFor: propTypes.string.isRequired,
  classList: propTypes.array,
  children: propTypes.any
};

export default Label;
