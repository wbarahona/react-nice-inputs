'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

var _FormGroup = require('./FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _Feedback = require('./Feedback');

var _Feedback2 = _interopRequireDefault(_Feedback);

var _Label = require('./Label');

var _Label2 = _interopRequireDefault(_Label);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Components


var DropDownDate = function (_Component) {
  _inherits(DropDownDate, _Component);

  function DropDownDate(props) {
    _classCallCheck(this, DropDownDate);

    // create a ref to store the thisInput DOM element
    var _this = _possibleConstructorReturn(this, (DropDownDate.__proto__ || Object.getPrototypeOf(DropDownDate)).call(this, props));

    _this.state = {
      classList: '',
      dayOptions: [],
      monthOptions: [],
      yearOptions: [],
      date: '',
      m: null,
      month: null,
      day: null,
      year: null
    };
    _this.defaults = {
      minDate: '01-01-1970',
      maxDate: '12-31-' + (0, _moment2.default)().year(),
      format: 'MM-DD-YYYY'
    };

    _this.prepopulateInput = function () {
      var _this$props = _extends({}, _this.props),
          value = _this$props.value;

      var _this$props2 = _this.props,
          propsFormat = _this$props2.format,
          propsMinDate = _this$props2.minDate,
          propsMaxDate = _this$props2.maxDate;
      var _this$defaults = _this.defaults,
          defaultFormat = _this$defaults.format,
          defaultMinDate = _this$defaults.minDate,
          defaultMaxDate = _this$defaults.maxDate;

      var format = propsFormat || defaultFormat;
      var minDate = propsMinDate || defaultMinDate;
      var maxDate = propsMaxDate || defaultMaxDate;
      var _mValDate = (0, _moment2.default)(value, format, true);
      var _mMinDate = (0, _moment2.default)(minDate, format, true);
      var _mMaxDate = (0, _moment2.default)(maxDate, format, true);

      if (value) {
        // if _mValDate is invalid then try rebuild it with default format
        if (!_mValDate.isValid()) {
          _mValDate = (0, _moment2.default)(value, defaultFormat, true);
        }
        // if _mMinDate is invalid then try rebuild it with default format
        if (!_mMinDate.isValid()) {
          _mMinDate = (0, _moment2.default)(minDate, defaultFormat, true);
        }
        // if _mMaxDate is invalid then try rebuild it with default format
        if (!_mMaxDate.isValid()) {
          _mMaxDate = (0, _moment2.default)(maxDate, defaultFormat, true);
        }

        // validate if value is a valid date format vs the format prop
        // validate against min dates and max dates to check if its within the selectable ranges

        if (_mValDate.isValid() && _mValDate.isBetween(_mMinDate, _mMaxDate)) {
          var daysInMonth = _mValDate.daysInMonth();

          _this.prepareDayOptions(1, daysInMonth);
          _this.setState({ date: value, m: _mValDate, month: _mValDate.month() + 1, day: _mValDate.date(), year: _mValDate.year() });
        } else {
          console.warn('\'react-nice-inputs\': The value prop is not matching the format: "' + format + '" or is not a valid date value or is out of min/max dates range.');
        }
      }
    };

    _this.prepareClassList = function () {
      var _this$props3 = _this.props,
          classList = _this$props3.classList,
          isValid = _this$props3.isValid,
          isInvalid = _this$props3.isInvalid,
          mmClassList = _this$props3.mmClassList,
          ddClassList = _this$props3.ddClassList,
          yyyyClassList = _this$props3.yyyyClassList;

      var mmClassListString = _utils2.default.createClassList(mmClassList);
      var ddClassListString = _utils2.default.createClassList(ddClassList);
      var yyyyClassListString = _utils2.default.createClassList(yyyyClassList);
      var classListString = _utils2.default.createClassList(classList, isValid, isInvalid);

      _this.setState({ classList: classListString, mmClassList: mmClassListString, ddClassList: ddClassListString, yyyyClassList: yyyyClassListString });
    };

    _this.prepareMonthOptions = function (firstMonth, lastMonth) {
      var mmmm = _this.props.mmmm;

      var monthOptions = [];

      for (var i = firstMonth; i <= lastMonth; ++i) {
        var opt = { value: i + 1 };

        opt.label = mmmm ? (0, _moment2.default)().month(i).format('MMMM') : i + 1;

        monthOptions.push(opt);
      }

      _this.setState({ monthOptions: monthOptions });
    };

    _this.prepareDayOptions = function (firstDay, lastDay) {
      var dayOptions = [];

      for (var i = firstDay; i <= lastDay; ++i) {
        var opt = { label: i, value: i };

        dayOptions.push(opt);
      }

      _this.setState({ dayOptions: dayOptions });
    };

    _this.prepareYearOptions = function () {
      var yearOptions = [];
      var _this$props4 = _this.props,
          propsFormat = _this$props4.format,
          propsMinDate = _this$props4.minDate,
          propsMaxDate = _this$props4.maxDate;
      var _this$defaults2 = _this.defaults,
          defaultFormat = _this$defaults2.format,
          defaultMinDate = _this$defaults2.minDate,
          defaultMaxDate = _this$defaults2.maxDate;

      var format = propsFormat || defaultFormat;
      var minDate = propsMinDate || defaultMinDate;
      var maxDate = propsMaxDate || defaultMaxDate;

      var _mMinDate = (0, _moment2.default)(minDate, format, true);
      var _mMaxDate = (0, _moment2.default)(maxDate, format, true);

      // if _mMinDate is invalid then try rebuild it with default format
      if (!_mMinDate.isValid()) {
        _mMinDate = (0, _moment2.default)(minDate, defaultFormat, true);
      }
      // if _mMaxDate is invalid then try rebuild it with default format
      if (!_mMaxDate.isValid()) {
        _mMaxDate = (0, _moment2.default)(maxDate, defaultFormat, true);
      }

      // const validDates = this.validateDates();
      var initialYear = _mMinDate.year();
      var finalYear = _mMaxDate.year();

      for (var year = finalYear; year >= initialYear; year--) {
        var opt = { label: year, value: year };

        yearOptions.push(opt);
      }

      _this.setState({ yearOptions: yearOptions }, function () {
        _this.checkDateRanges();
      });
    };

    _this.validateDates = function () {
      var _this$props5 = _extends({}, _this.props),
          propsMinDate = _this$props5.minDate,
          propsMaxDate = _this$props5.maxDate,
          propsFormat = _this$props5.format;

      var _this$defaults3 = _this.defaults,
          defaultMinDate = _this$defaults3.minDate,
          defaultMaxDate = _this$defaults3.maxDate;

      var response = {
        minDateValid: true,
        maxDateValid: true
      };
      var minDate = '';
      var maxDate = '';
      var format = '';

      minDate = propsMinDate || defaultMinDate;
      maxDate = propsMaxDate || defaultMaxDate;

      if (propsFormat && (propsMinDate || propsMaxDate)) {
        format = propsFormat;

        response.minDateValid = (0, _moment2.default)(minDate, format, true).isValid();
        response.maxDateValid = (0, _moment2.default)(maxDate, format, true).isValid();
      }

      if (!response.minDateValid && propsMinDate && propsFormat) {
        console.warn('\'react-nice-inputs\': Dates format provided missmatches the format props, resorting to default dates, please send "minDate" prop following the format: ' + propsFormat);
      }
      if (!response.maxDateValid && propsMaxDate && propsFormat) {
        console.warn('\'react-nice-inputs\': Dates format provided missmatches the format props, resorting to default dates, please send "maxDate" prop following the format: ' + propsFormat);
      }

      return response;
    };

    _this.prepareFragmentedDateRanges = function () {
      var dateRanges = {
        minDate: {
          month: null,
          day: null,
          year: null
        },
        maxDate: {
          month: null,
          day: null,
          year: null
        }
      };
      // const { minDate, maxDate, format } = this.props;

      var _this$props6 = _extends({}, _this.props),
          propsFormat = _this$props6.format,
          propsMinDate = _this$props6.minDate,
          propsMaxDate = _this$props6.maxDate;

      var _this$defaults4 = _this.defaults,
          defaultFormat = _this$defaults4.format,
          defaultMinDate = _this$defaults4.minDate,
          defaultMaxDate = _this$defaults4.maxDate;

      var format = propsFormat || defaultFormat;
      var minDate = propsMinDate || defaultMinDate;
      var maxDate = propsMaxDate || defaultMaxDate;
      var _mMinDate = (0, _moment2.default)(minDate, format, true);
      var _mMaxDate = (0, _moment2.default)(maxDate, format, true);

      // if _mMinDate is invalid then try rebuild it with default format
      if (!_mMinDate.isValid()) {
        _mMinDate = (0, _moment2.default)(minDate, defaultFormat, true);
      }
      // if _mMaxDate is invalid then try rebuild it with default format
      if (!_mMaxDate.isValid()) {
        _mMaxDate = (0, _moment2.default)(maxDate, defaultFormat, true);
      }

      dateRanges.minDate.month = _mMinDate.month();
      dateRanges.minDate.day = _mMinDate.date();
      dateRanges.minDate.year = _mMinDate.year();

      dateRanges.maxDate.month = _mMaxDate.month();
      dateRanges.maxDate.day = _mMaxDate.date();
      dateRanges.maxDate.year = _mMaxDate.year();

      return dateRanges;
    };

    _this.buildDate = function (value, concept, group) {
      var stateCopy = _extends({}, _this.state);

      stateCopy[group] = value !== '' ? parseInt(value, 10) : null;

      _this.setState({ month: stateCopy.month, day: stateCopy.day, year: stateCopy.year }, function () {
        var _this$props7 = _extends({}, _this.props),
            propsFormat = _this$props7.format,
            propsMinDate = _this$props7.minDate,
            propsMaxDate = _this$props7.maxDate;

        var _this$defaults5 = _this.defaults,
            defaultFormat = _this$defaults5.format,
            defaultMinDate = _this$defaults5.minDate,
            defaultMaxDate = _this$defaults5.maxDate;


        var monthValue = stateCopy.month;
        var dayValue = stateCopy.day;
        var yearValue = stateCopy.year;

        var monthConcept = 'MM';
        var yearConcept = 'YYYY';
        var dateRanges = _this.prepareFragmentedDateRanges();
        var format = propsFormat || defaultFormat;
        var minDate = propsMinDate || defaultMinDate;
        var maxDate = propsMaxDate || defaultMaxDate;
        var _mMinDate = (0, _moment2.default)(minDate, format, true);
        var _mMaxDate = (0, _moment2.default)(maxDate, format, true);
        var jsdate = null;
        var date = null;
        var dateFormat = '';
        var daysInMonth = 0;

        // if _mMinDate is invalid then try rebuild it with default format
        if (!_mMinDate.isValid()) {
          _mMinDate = (0, _moment2.default)(minDate, defaultFormat, true);
        }
        // if _mMaxDate is invalid then try rebuild it with default format
        if (!_mMaxDate.isValid()) {
          _mMaxDate = (0, _moment2.default)(maxDate, defaultFormat, true);
        }

        if (monthValue !== '') {
          date = monthValue;
          dateFormat = monthConcept;
        }
        if (monthValue !== '' && yearValue !== '' && !isNaN(yearValue)) {
          date = yearValue + '-' + monthValue;
          dateFormat = yearConcept + '-' + monthConcept;
        }

        daysInMonth = (0, _moment2.default)(date, dateFormat).daysInMonth(); // does not need true as third param

        _this.prepareDayOptions(1, daysInMonth);

        // Check if years picked correspond to either max year or min year and rebuild month options
        if (dateRanges.minDate.year < yearValue && yearValue < dateRanges.maxDate.year) {
          _this.prepareMonthOptions(0, 11);
        } else if (yearValue === dateRanges.minDate.year) {
          _this.prepareMonthOptions(dateRanges.minDate.month, 11);
        } else if (yearValue === dateRanges.maxDate.year) {
          _this.prepareMonthOptions(0, dateRanges.maxDate.month);
        }

        // Check if years picked and month picked correspond to either max year or min year and rebuild days options
        if (yearValue === dateRanges.minDate.year && monthValue - 1 === dateRanges.minDate.month) {
          _this.prepareDayOptions(dateRanges.minDate.day, daysInMonth);
        } else if (yearValue === dateRanges.maxDate.year && monthValue - 1 === dateRanges.maxDate.month) {
          _this.prepareDayOptions(1, dateRanges.maxDate.day);
        }

        if (monthValue !== '' && dayValue !== '' && yearValue !== '' && monthValue !== null && dayValue !== null && yearValue !== null && !isNaN(monthValue) && !isNaN(dayValue) && !isNaN(yearValue)) {
          jsdate = new Date(yearValue, monthValue - 1, dayValue);
          date = (0, _moment2.default)(jsdate).format(format);
          var _mValDate = (0, _moment2.default)(date, format, true);

          // if _mValDate is invalid then try rebuild it with default format
          if (!_mValDate.isValid()) {
            _mValDate = (0, _moment2.default)(date, defaultFormat);
          }

          var withinDateRange = _mValDate.isBetween(_mMinDate, _mMaxDate, false, true);

          if (withinDateRange) {
            _this.setState({ date: date, m: (0, _moment2.default)(jsdate), month: monthValue, day: dayValue, year: yearValue });
          } else {
            _this.setState({ date: '' });
          }
        } else {
          _this.setState({ date: '' });
        }
      });
    };

    _this.handleChange = function () {
      _this.props.onChange(_this.state.date, _this.props.name, _this.thisInput.current, _this.state.m);
    };

    _this.handlePick = function (value, name, e) {
      var el = e.currentTarget;
      var group = el.getAttribute('data-group');
      var concept = el.getAttribute('data-concept');

      _this.buildDate(value, concept, group);
    };

    _this.checkDateRanges = function () {
      var _this$props8 = _this.props,
          propsFormat = _this$props8.format,
          propsMinDate = _this$props8.minDate,
          propsMaxDate = _this$props8.maxDate;
      var _this$defaults6 = _this.defaults,
          defaultFormat = _this$defaults6.format,
          defaultMinDate = _this$defaults6.minDate,
          defaultMaxDate = _this$defaults6.maxDate;

      var format = propsFormat || defaultFormat;
      var minDate = propsMinDate || defaultMinDate;
      var maxDate = propsMaxDate || defaultMaxDate;
      var _mValDate = (0, _moment2.default)(_this.state.date, format, true);
      var _mMinDate = (0, _moment2.default)(minDate, format, true);
      var _mMaxDate = (0, _moment2.default)(maxDate, format, true);

      var withinDateRange = _mValDate.isBetween(_mMinDate, _mMaxDate, false, true);

      if (!withinDateRange) {
        _this.resetComponent();
      }
    };

    _this.resetComponent = function () {
      _this.prepareDayOptions(0, 31);
      _this.prepareMonthOptions(0, 11);
      _this.setState({
        date: '',
        month: null,
        day: null,
        year: null
      });
    };

    _this.thisInput = _react2.default.createRef();
    return _this;
  }

  _createClass(DropDownDate, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
      this.prepareMonthOptions(0, 11);
      this.prepareYearOptions();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }
      if (prevProps.minDate !== this.props.minDate || prevProps.maxDate !== this.props.maxDate) {
        this.prepareYearOptions();
      }
      if (prevProps.mmmm !== this.props.mmmm) {
        this.prepareMonthOptions(0, 11);
      }
      if (this.state.date !== null && prevState.date !== this.state.date) {
        this.handleChange(this.thisInput);
      }
      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        if ((prevProps.value !== this.props.value || this.props.value !== this.state.date) && this.state.date === '') {
          this.prepopulateInput();
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          name = _props.name,
          label = _props.label,
          feedback = _props.feedback,
          defaultTextM = _props.defaultTextM,
          defaultTextD = _props.defaultTextD,
          defaultTextY = _props.defaultTextY,
          labelM = _props.labelM,
          labelD = _props.labelD,
          labelY = _props.labelY,
          attrs = _props.attrs;
      var _state = this.state,
          date = _state.date,
          month = _state.month,
          day = _state.day,
          year = _state.year,
          classList = _state.classList,
          mmClassList = _state.mmClassList,
          ddClassList = _state.ddClassList,
          yyyyClassList = _state.yyyyClassList,
          monthOptions = _state.monthOptions,
          dayOptions = _state.dayOptions,
          yearOptions = _state.yearOptions;


      return _react2.default.createElement(
        'div',
        { className: classList + ' ddd-wrap' },
        label && _react2.default.createElement(
          _Label2.default,
          { htmlFor: name },
          label
        ),
        _react2.default.createElement('input', _extends({ ref: this.thisInput, type: 'hidden', name: name, id: name, value: date }, attrs)),
        _react2.default.createElement(
          'div',
          { className: mmClassList + ' month-select' },
          _react2.default.createElement(_FormGroup2.default, { type: 'select',
            name: 'mm-' + name,
            onChange: this.handlePick,
            label: labelM,
            options: monthOptions,
            defaultText: defaultTextM,
            attrs: { 'data-concept': 'MM', 'data-group': 'month' },
            value: month
          })
        ),
        _react2.default.createElement(
          'div',
          { className: ddClassList + ' day-select' },
          _react2.default.createElement(_FormGroup2.default, { type: 'select',
            name: 'dd-' + name,
            onChange: this.handlePick,
            label: labelD,
            options: dayOptions,
            defaultText: defaultTextD,
            attrs: { 'data-concept': 'DD', 'data-group': 'day' },
            value: day
          })
        ),
        _react2.default.createElement(
          'div',
          { className: yyyyClassList + ' year-select' },
          _react2.default.createElement(_FormGroup2.default, { type: 'select',
            name: 'yyyy-' + name,
            onChange: this.handlePick,
            label: labelY,
            options: yearOptions,
            defaultText: defaultTextY,
            attrs: { 'data-concept': 'YYYY', 'data-group': 'year' },
            value: year
          })
        ),
        feedback && _react2.default.createElement(_Feedback2.default, { copy: feedback })
      );
    }
  }]);

  return DropDownDate;
}(_react.Component);

DropDownDate.propTypes = {
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array.isRequired,
  attrs: _propTypes2.default.object,
  onChange: _propTypes2.default.func.isRequired,
  defaultTextM: _propTypes2.default.string,
  defaultTextD: _propTypes2.default.string,
  defaultTextY: _propTypes2.default.string,
  labelM: _propTypes2.default.string,
  labelD: _propTypes2.default.string,
  labelY: _propTypes2.default.string,
  label: _propTypes2.default.string,
  feedback: _propTypes2.default.node,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  mmmm: _propTypes2.default.bool,
  format: _propTypes2.default.string,
  minDate: _propTypes2.default.string,
  maxDate: _propTypes2.default.string,
  value: _propTypes2.default.string,
  mmClassList: _propTypes2.default.array,
  ddClassList: _propTypes2.default.array,
  yyyyClassList: _propTypes2.default.array
};

DropDownDate.defaultProps = {
  defaultTextM: 'Select a Month...',
  defaultTextD: 'Select a Day...',
  defaultTextY: 'Select a Year...',
  labelM: 'Month:',
  labelD: 'Day:',
  labelY: 'Year:',
  mmmm: false,
  mmClassList: [],
  ddClassList: [],
  yyyyClassList: []
};

exports.default = DropDownDate;