import React, { Component } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Select extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
  }

  state = {
    classList: '',
    value: ''
  }

  componentDidMount() {
    this.prepareClassList();
    this.prepopulateInput();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
      this.prepareClassList();
    }

    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if (prevProps.value !== this.props.value) {
        this.prepopulateInput();
      }
    }
  }

  prepopulateInput = () => {
    const { value } = { ... this.props };

    if (value) {
      this.setState({ value });
    } else {
      this.setState({ value: '' });
    }
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid } = this.props;
    const classListString = utils.createClassList(classList, isValid, isInvalid);

    this.setState({ classList: classListString });
  }

  handleChange = e => {
    const { name } = this.props;
    const el = e.target;
    const value = el.value;

    this.setState({ value });

    this.props.onChange(value, name, e);
  }

  render() {
    const { name, attrs, options, defaultText, noDefault } = this.props;
    const { classList, value } = this.state;

    return (
      <div className={ `${ classList } select-wrap` }>
        <select ref={ this.thisInput } name={ name } id={ name } className="input" onChange={ this.handleChange } value={ value } { ...attrs }>
          { !noDefault && <option value="">{ defaultText }</option> }
          {
            options.map(opt => (
              <option key={ opt.value } value={ opt.value } { ... opt.attrs }>{ opt.label }</option>
            ))
          }
        </select>
      </div>
    );
  }
}

Select.propTypes = {
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  attrs: propTypes.object,
  onChange: propTypes.func.isRequired,
  options: propTypes.array.isRequired,
  defaultText: propTypes.string,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  noDefault: propTypes.bool,
  value: propTypes.oneOfType([ propTypes.string, propTypes.number ]),
};

Select.defaultProps = {
  defaultText: 'Select an option:  '
};

export default Select;
