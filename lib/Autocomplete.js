'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Autocomplete = function (_Component) {
  _inherits(Autocomplete, _Component);

  function Autocomplete(props) {
    _classCallCheck(this, Autocomplete);

    // create a ref to store the thisInput DOM element
    var _this = _possibleConstructorReturn(this, (Autocomplete.__proto__ || Object.getPrototypeOf(Autocomplete)).call(this, props));

    _this.state = {
      classList: '',
      dataList: [],
      inputName: null,
      value: '',
      maskValue: '',
      optionIndex: -1,
      optionsVisible: false
    };

    _this.registerMouseDown = function () {
      document.addEventListener('mousedown', _this.handleClickOutside);
      document.addEventListener('touchstart', _this.handleClickOutside);
    };

    _this.unRegisterMouseDown = function () {
      document.removeEventListener('mousedown', _this.handleClickOutside);
      document.removeEventListener('touchstart', _this.handleClickOutside);
    };

    _this.handleClickOutside = function (event) {
      var thisAutocompleteList = _this.thisOptions.current;

      if (_this.thisMask && !_this.thisMask.current.contains(event.target) && thisAutocompleteList) {
        // check if this.state.value correspond to any value in the options prop
        if (!thisAutocompleteList.contains(event.target)) {
          var found = _this.state.dataList.filter(function (e) {
            return e.label === _this.state.maskValue;
          })[0];

          // this.toggleAction();
          _this.closeOptions();

          if (!found) {
            _this.setState({ value: '', maskValue: '' }, function () {
              _this.resetOptions();
              _this.handleChange(_this.thisInput);
            });
          }
        }
      }
    };

    _this.resetOptions = function () {
      var dataListCopy = [].concat(_toConsumableArray(_this.props.dataList));

      _this.setState({ dataList: dataListCopy });
    };

    _this.prepopulateInput = function () {
      var _this$props = _extends({}, _this.props),
          value = _this$props.value,
          dataList = _this$props.dataList;

      var option = dataList.filter(function (opt) {
        return opt.value === value;
      })[0];

      if (value && option) {
        var maskValue = option.label;

        _this.setState({ value: value, maskValue: maskValue });
      } else if (value === '') {
        _this.setState({ value: '', maskValue: '' });
      }
    };

    _this.prepareClassList = function () {
      var _this$props2 = _this.props,
          classList = _this$props2.classList,
          isValid = _this$props2.isValid,
          isInvalid = _this$props2.isInvalid;

      var classListString = _utils2.default.createClassList(classList, isValid, isInvalid);

      classListString = 'input ' + classListString;

      _this.setState({ classList: classListString });
    };

    _this.prepareDataList = function () {
      var dataList = _this.props.dataList;


      _this.setState({ dataList: dataList });
    };

    _this.prepareForeignEvents = function () {
      var maskInput = _this.thisMask.current;

      maskInput.onkeyup = function (e) {
        _this.navigateThroughOptions(e.keyCode);
      };
    };

    _this.navigateThroughOptions = function (keyCode) {
      // 40 up - 38 down - 13 enter
      var thisAutocompleteList = _this.thisOptions.current;

      if (thisAutocompleteList) {
        var options = thisAutocompleteList.children;


        if (options) {
          var optionsLen = options.length;
          var optionIndex = _this.state.optionIndex;


          if (optionsLen > 0 && (keyCode === 40 || keyCode === 38)) {
            _this.clearOptionSelections();

            if (keyCode === 40 && optionIndex < optionsLen - 1) {
              optionIndex++;
            } else if (keyCode === 38 && optionIndex > 0) {
              optionIndex--;
            }
            if (options[optionIndex] !== undefined) {
              options[optionIndex].classList.add('active');
            }
            _this.setState({ optionIndex: optionIndex });
          } else if (optionsLen > 0 && (keyCode === 13 || keyCode === 9) && options[optionIndex] !== undefined) {
            _this.setAutocompleteValue(options[optionIndex]);
            _this.clearOptionSelections();
          }
        }
      }
    };

    _this.clearOptionSelections = function () {
      var thisAutocompleteList = _this.thisOptions.current;

      if (thisAutocompleteList) {
        var options = thisAutocompleteList.children;


        if (options) {
          for (var opt in options) {
            if (options.hasOwnProperty(opt)) {
              var option = options[opt];

              option.classList.remove('active');
            }
          }
        }
      }
    };

    _this.matches = function (searchString) {
      var dataListCopy = [].concat(_toConsumableArray(_this.props.dataList));
      var matches = dataListCopy.filter(function (item) {
        return item.label.toLowerCase().includes(searchString.toLowerCase());
      });

      _this.setState({ dataList: matches });

      return matches;
    };

    _this.toggleAction = function () {
      _this.setState({ optionsVisible: !_this.state.optionsVisible });
    };

    _this.closeOptions = function () {
      _this.setState({ optionsVisible: false });
    };

    _this.openOptions = function () {
      _this.setState({ optionsVisible: true });
    };

    _this.toggleList = function (e) {
      var elem = e.currentTarget;
      var name = elem.name;

      _this.setState({ inputName: name }, function () {
        _this.toggleAction();
      });
    };

    _this.selectOption = function (e) {
      _this.setAutocompleteValue(e.currentTarget);
    };

    _this.changeMask = function (e) {
      var currentTarget = e.currentTarget;
      var value = currentTarget.value;


      _this.setState({ maskValue: value }, function () {
        _this.matches(_this.state.maskValue);

        if (_this.state.maskValue.length <= 0) {
          _this.setState({ value: '', optionsVisible: false });
        } else {
          _this.setState({ optionsVisible: true });
        }
      });
    };

    _this.setAutocompleteValue = function (el) {
      var maskValue = el.getAttribute('data-label');
      var value = el.getAttribute('data-value');

      _this.toggleAction();
      _this.setState({ optionIndex: -1, maskValue: maskValue, value: value });
    };

    _this.handleChange = function (e) {
      var name = _this.thisInput.current.name;
      var value = _this.state.value;

      return _this.props.onChange(value, name, e);
    };

    _this.thisInput = _react2.default.createRef();
    _this.thisMask = _react2.default.createRef();
    _this.thisOptions = _react2.default.createRef();
    return _this;
  }

  _createClass(Autocomplete, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
      this.prepareDataList();
      this.prepareForeignEvents();
      this.prepopulateInput();
      this.registerMouseDown();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (prevProps.classList && this.props.classList) {
        if (prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
          this.prepareClassList();
        }
      }
      if (this.state.value !== null && prevState.value !== this.state.value) {
        this.handleChange(this.thisInput);
      }

      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        if (prevProps.value !== this.props.value) {
          this.prepopulateInput();
        }
      }
      if (prevProps.dataList.length < this.props.dataList.length) {
        this.prepareDataList();

        if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
          this.prepopulateInput();
        }
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unRegisterMouseDown();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          attrs = _props.attrs,
          name = _props.name;
      var _state = this.state,
          classList = _state.classList,
          dataList = _state.dataList,
          optionsVisible = _state.optionsVisible;


      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement('input', { ref: this.thisInput, type: 'hidden', name: name, id: 'hidden-autocomplete-' + name, value: this.state.value }),
        _react2.default.createElement('input', _extends({ ref: this.thisMask, type: 'text', name: name, id: name, className: classList, onChange: this.changeMask, onFocus: this.toggleList, value: this.state.maskValue }, attrs)),
        optionsVisible && _react2.default.createElement(
          'ul',
          { ref: this.thisOptions, className: 'autocomplete-list', id: 'dropdown-autocomplete-' + name },
          dataList.map(function (item, index) {
            return _react2.default.createElement(
              'li',
              { key: item.value, 'data-value': item.value, 'data-label': item.label, 'data-count': index, onClick: _this2.selectOption },
              item.label
            );
          })
        )
      );
    }
  }]);

  return Autocomplete;
}(_react.Component);

Autocomplete.propTypes = {
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array.isRequired,
  attrs: _propTypes2.default.object,
  onChange: _propTypes2.default.func.isRequired,
  dataList: _propTypes2.default.array.isRequired,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  value: _propTypes2.default.string,
  optionTemplate: _propTypes2.default.node
};

exports.default = Autocomplete;