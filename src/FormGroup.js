import React, { Component } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

// Components
import Feedback from './Feedback';
import Icon from './Icon';
import Label from './Label';
import Input from './Input';
import InputGroup from './InputGroup';
import Select from './Select';
import Autocomplete from './Autocomplete';
import InputMask from './InputMask';

class FormGroup extends Component {
  state = {
    boxClassList: ''
  }

  componentDidMount() {
    this.prepareClassList();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.boxClassList.length < this.props.boxClassList.length || utils.checkClassListNames(prevProps.boxClassList, this.props.boxClassList)) {
      this.prepareClassList();
    }
  }

  prepareClassList = () => {
    const { boxClassList } = this.props;

    if (boxClassList.length <= 0) {
      boxClassList.push('full-width');
    }
    const boxClassListString = utils.createClassList(boxClassList);

    this.setState({ boxClassListString });
  }

  render() {
    const { type, name, attrs, feedback, icon, label, classList, onChange, isValid, isInvalid, defaultText, options, dataList, boxClassList, optionTemplate, value, mask, noDefault } = this.props;
    const { boxClassListString } = this.state;

    return (
      <div className={ boxClassListString }>
        <div className="form-group" >
          { label && <Label htmlFor={ name }>{ label }</Label> }
          <div className="input-wrap">
            {(() => {
              switch(type) {
                case 'textarea':
                  return (
                    <Input type={ type }
                      name={ name }
                      classList={ classList }
                      onChange={ onChange }
                      attrs={ attrs }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      value={ value }
                    />
                  );
                case 'radio':
                case 'checkbox':
                  return (
                    <InputGroup type={ type }
                      name={ name }
                      onChange={ onChange }
                      classList={ classList }
                      options={ options }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      value={ value }
                    />
                  );
                case 'select':
                  return (
                    <Select name={ name }
                      classList={ classList }
                      onChange={ onChange }
                      attrs={ attrs }
                      options={ options }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      defaultText={ defaultText }
                      value={ value }
                      noDefault={ noDefault }
                    />
                  );
                case 'autocomplete':
                  return (
                    <Autocomplete name={ name }
                      classList={ classList }
                      onChange={ onChange }
                      attrs={ attrs }
                      dataList={ dataList }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      boxClassList={ boxClassList }
                      optionTemplate={ optionTemplate }
                      value={ value }
                    />
                  );
                case 'masked':
                  return (
                    <InputMask name={ name }
                      classList={ classList }
                      onChange={ onChange }
                      attrs={ attrs }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      mask={ mask }
                      value={ value }
                    />
                  );
                default:
                  return (
                    <Input type={ type }
                      name={ name }
                      classList={ classList }
                      onChange={ onChange }
                      attrs={ attrs }
                      isValid={ isValid }
                      isInvalid={ isInvalid }
                      value={ value }
                    />
                  );
              }
            })()}
            { icon && <Icon classList={ icon } /> }
            { feedback && <Feedback>{ feedback }</Feedback> }
          </div>
        </div>
      </div>
    );
  }
}

FormGroup.propTypes = {
  type: propTypes.string.isRequired,
  name: propTypes.string.isRequired,
  classList: propTypes.array,
  onChange: propTypes.func.isRequired,
  attrs: propTypes.object,
  feedback: propTypes.node,
  icon: propTypes.array,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  label: propTypes.node,
  defaultText: propTypes.string,
  options: propTypes.array,
  dataList: propTypes.array,
  boxClassList: propTypes.array,
  optionTemplate: propTypes.node,
  value: propTypes.oneOfType([ propTypes.string, propTypes.array, propTypes.number ]),
  mask: propTypes.string,
  noDefault: propTypes.bool
};

FormGroup.defaultProps = {
  defaultText: 'Select an option...',
  options: [],
  dataList: [],
  classList: [],
  boxClassList: []
};

export default FormGroup;
