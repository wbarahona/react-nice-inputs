'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var utils = {};

utils.debounce = function (fn, delay) {
  var timer = null;

  return function ret() {
    var self = this;
    var args = arguments;

    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(self, args);
    }, delay);
  };
};

utils.createClassList = function (classListProp, isValid, isInvalid) {
  var classList = '';

  if (classListProp && classListProp.length > 0) {
    var classListCopy = [].concat(_toConsumableArray(classListProp));

    if (isValid) {
      classListCopy.push('is-valid');
      var idx = classListCopy.indexOf('is-invalid');

      classListCopy.slice(idx, 1);
    } else {
      var _idx = classListCopy.indexOf('is-valid');

      classListCopy.slice(_idx, 1);
    }

    if (isInvalid) {
      classListCopy.push('is-invalid');
      var _idx2 = classListCopy.indexOf('is-valid');

      classListCopy.slice(_idx2, 1);
    } else {
      var _idx3 = classListCopy.indexOf('is-invalid');

      classListCopy.slice(_idx3, 1);
    }

    classList = classListCopy.join(' ', ',');
  }

  return classList;
};

utils.checkClassListNames = function (prevClassList, currClassList) {
  for (var i = 0; i < prevClassList.length; i++) {
    var prevClassListName = prevClassList[i];
    var currClassListName = currClassList[i];

    if (prevClassListName !== currClassListName) {
      return true;
    }
  }

  return false;
};

utils.findInOptions = function (options, value) {
  return options.find(function (e) {
    return e.value === value;
  });
};

exports.default = utils;