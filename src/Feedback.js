import React from 'react';
import propTypes from 'prop-types';

const Feedback = ({ children }) => {
  return(
    <p className="form-feedback">{ children }</p>
  );
};

Feedback.propTypes = {
  children: propTypes.any
};

export default Feedback;
