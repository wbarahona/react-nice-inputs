'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_Component) {
  _inherits(Input, _Component);

  function Input(props) {
    _classCallCheck(this, Input);

    // create a ref to store the thisInput DOM element
    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this.state = {
      classList: '',
      value: ''
    };

    _this.prepopulateInput = function () {
      var _this$props = _extends({}, _this.props),
          value = _this$props.value;

      if (value) {
        _this.setState({ value: value });
      } else {
        _this.setState({ value: '' });
      }
    };

    _this.prepareClassList = function () {
      var _this$props2 = _this.props,
          classList = _this$props2.classList,
          isValid = _this$props2.isValid,
          isInvalid = _this$props2.isInvalid;

      var classListString = _utils2.default.createClassList(classList, isValid, isInvalid);

      classListString = 'input ' + classListString;

      _this.setState({ classList: classListString });
    };

    _this.handleChange = function (e) {
      var name = _this.props.name;

      var value = e.target.value;

      _this.setState({ value: value });
      _this.props.onChange(value, name, e);
    };

    _this.thisInput = _react2.default.createRef();
    return _this;
  }

  _createClass(Input, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
      this.prepopulateInput();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (prevProps.classList && this.props.classList && prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }

      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        if (prevProps.value !== this.props.value) {
          this.prepopulateInput();
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          type = _props.type,
          name = _props.name,
          attrs = _props.attrs;
      var _state = this.state,
          classList = _state.classList,
          value = _state.value;


      return _react2.default.createElement(
        _react.Fragment,
        null,
        function () {
          switch (type) {
            case 'textarea':
              return _react2.default.createElement('textarea', _extends({ ref: _this2.thisInput, name: name, id: name, className: classList, onChange: _this2.handleChange, value: value }, attrs));
            default:
              return _react2.default.createElement('input', _extends({ ref: _this2.thisInput, type: type, name: name, id: name, className: classList, onChange: _this2.handleChange, value: value }, attrs));
          }
        }()
      );
    }
  }]);

  return Input;
}(_react.Component);

Input.propTypes = {
  type: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array.isRequired,
  attrs: _propTypes2.default.object,
  onChange: _propTypes2.default.func.isRequired,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  value: _propTypes2.default.string
};

exports.default = Input;