import React, { Component, Fragment } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Input extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
    this.thisMask = React.createRef();
  }

  state = {
    classList: '',
    fill: '*****',
    what2show: '',
    maskValue: '',
    inputValue: ''
  }

  componentDidMount() {
    this.prepareClassList();
    this.prepopulateInput();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.classList && this.props.classList && prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
      this.prepareClassList();
    }

    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if (prevProps.value !== this.props.value) {
        this.prepopulateInput();
      }
    }
  }

  prepopulateInput = () => {
    const { value, name } = { ... this.props };
    const valAsStr = (value) ? `${ value }` : '';


    if (value && valAsStr !== '') {
      this.setState({ inputValue: valAsStr, maskValue: valAsStr }, () => {
        this.mask();
        this.setState({ maskValue: this.state.what2show }, () => {
          this.onBlur();
          this.handleChange(value, name, this.thisInput.current);
        });
      });
    }
    // if (typeof value === 'string' && valAsStr === '') {
    //   this.setState({ inputValue: '', maskValue: '', what2show: '' }, () => {
    //     const thisInput = this.thisInput.current;

    //     this.handleChange(value, name, thisInput);
    //   });
    // }
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid } = this.props;
    let classListString = utils.createClassList(classList, isValid, isInvalid);

    classListString = `input ${ classListString }`;

    this.setState({ classList: classListString });
  }

  handleMaskChange = e => {
    const el = e.target;
    const value = el.value;
    const thisInput = this.thisInput.current;
    const { name } = this.props;

    this.setState({ inputValue: value, maskValue: value }, () => {
      this.handleChange(value, name, thisInput);
      this.mask();
      // this.workFormat();
    });
  }

  handleChange = (value, name, e) => {
    this.props.onChange(value, name, e);
  }

  workFormat = () => {
    const { cc } = this.props;

    if (cc && this.state.inputValue.length > 0 && this.state.inputValue.length < 17) {
      const maskValue = this.state.inputValue.match(new RegExp('.{1,4}', 'g')).join('-');

      console.log(maskValue);

      // this.setState({ maskValue });
    }
  }

  onBlur = () => {
    // get mask here and POP mask it
    const { isInvalid } = this.props;
    const negatedValidity = isInvalid || false;
    const finalValue = (negatedValidity) ? this.state.maskValue : this.state.what2show;

    this.setState({ maskValue: finalValue });
  }

  onFocus = () => {
    // clear both inputs
    this.setState({ inputValue: '', maskValue: '', what2show: '' }, () => {
      const thisInput = this.thisInput.current;
      const { name } = this.props;
      const { inputValue: value } = this.state;

      this.handleChange(value, name, thisInput);
    });
  }

  mask = () => {
    const { inputValue } = this.state;
    const { fill } = this.state;
    const { mask, last, type, cc, cvv = false } = this.props;
    const regex = new RegExp(mask || '.*', 'i');

    // if (inputValue === '') { return false; }
    if (inputValue.indexOf(fill) === -1 && regex.test(inputValue)) {
      // Matches the regex? else put blank
      let what2show = (inputValue.length !== inputValue.match(regex)[0].length) ? inputValue.match(regex)[0] : '';

      // const ccrep = inputValue.replace(/\d(?=\d{4})/g, '*');

      // console.log(ccrep);

      // Replace start or end?
      what2show = (last) ? `${ fill }${ what2show }` : `${ what2show}${ fill }`;

      // is type email? mask but show first letter
      what2show = (type === 'email') ? `${ inputValue[0] }${ fill }${ what2show.replace(fill, '') }` : what2show;

      // is CC? show last 4 digits
      what2show = (cc) ? inputValue.replace(/\d(?=\d{4})/g, '•') : what2show;

      // is cvv? replace characters with bullets
      what2show = (cvv) ? inputValue.replace(/./g,'•') : what2show;

      this.setState({ what2show });
    } else {
      this.setState({ what2show: inputValue });
    }
  }

  render() {
    const { name, attrs } = this.props;
    const { classList, inputValue, maskValue } = this.state;
    const type = 'text';

    return (
      <Fragment>
        <input ref={ this.thisInput } type="hidden" name={ name } id={ name } value={ inputValue } onChange={ this.handleChange } />
        <input ref={ this.thisMask } type={ type } name={ name } className={ classList } onChange={ this.handleMaskChange } onFocus={ this.onFocus } onBlur={ this.onBlur } { ... attrs } value={ maskValue } />
      </Fragment>
    );
  }
}

Input.propTypes = {
  type: propTypes.string.isRequired,
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  mask: propTypes.string,
  cc: propTypes.bool,
  attrs: propTypes.object,
  onChange: propTypes.func.isRequired,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  value: propTypes.string,
  last: propTypes.any,
  cvv: propTypes.bool
};

export default Input;
