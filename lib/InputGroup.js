'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

var _Label = require('./Label');

var _Label2 = _interopRequireDefault(_Label);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Components


var InputGroup = function (_Component) {
  _inherits(InputGroup, _Component);

  function InputGroup(props) {
    _classCallCheck(this, InputGroup);

    // create a ref to store the thisInput DOM element
    var _this = _possibleConstructorReturn(this, (InputGroup.__proto__ || Object.getPrototypeOf(InputGroup)).call(this, props));

    _this.state = {
      classList: '',
      checked: {},
      values: [],
      value: ''
    };

    _this.compareArrays = function (prevValue, currentValue) {
      return prevValue.length === currentValue.length && prevValue.sort().every(function (value, index) {
        return value === currentValue.sort()[index];
      });
    };

    _this.resetInputs = function () {
      var checked = [];

      _this.props.options.map(function (opt) {
        // const chk = { status: value.includes(opt.value) };
        var chk = { status: false };

        checked[opt.value] = _extends({}, chk);
      });
      _this.setState({ values: [], value: '', checked: checked });
    };

    _this.buildChecked = function () {
      var _this$props = _this.props,
          options = _this$props.options,
          value = _this$props.value,
          type = _this$props.type;

      var checked = {};
      var values = [];
      var valueString = '';

      // set all options to unchecked
      options.map(function (opt) {
        // const chk = { status: value.includes(opt.value) };
        var chk = { status: false };

        checked[opt.value] = _extends({}, chk);

        if (value.includes(opt.value)) {
          values.push(opt.value);
        }
      });

      valueString = values.join(',');

      for (var i = 0; i < value.length; i++) {
        var val = value[i];

        if (checked[val]) {
          checked[val].status = true;
        }
        if (type === 'radio') {
          valueString = values[0];
          if (values.length >= 2) {
            console.warn('react-nice-inputs: we detected you are using "radio" buttons and sending the value props with more than one element, using the last element from value array as default');
          }

          break;
        }
      }

      _this.setState({ checked: checked, values: values, value: valueString });
    };

    _this.prepareClassList = function () {
      var _this$props2 = _this.props,
          classList = _this$props2.classList,
          isValid = _this$props2.isValid,
          isInvalid = _this$props2.isInvalid;

      var classListCopy = [].concat(_toConsumableArray(classList), ['input-list-wrap']);
      var wrapClassListString = _utils2.default.createClassList(classListCopy, isValid, isInvalid);

      _this.setState({ classList: wrapClassListString });
    };

    _this.prepareCheckedItems = function (value, type) {
      var stateCopy = _extends({}, _this.state);
      var checked = stateCopy.checked;

      var values = [];

      if (type === 'checkbox') {
        for (var item in checked) {
          if (checked.hasOwnProperty(item)) {
            var el = checked[item];

            if (el.status) {
              values.push(item);
            }
          }
        }
      } else {
        values = [value];
      }

      return {
        values: values,
        value: values.join(',')
      };
    };

    _this.prepareValues = function (val, name, type, e) {
      var stateCopy = _extends({}, _this.state);
      var checked = stateCopy.checked;

      var checkedLen = Object.keys(checked).length;

      if (checkedLen > 0) {
        checked[val].status = !checked[val].status;

        var _this$prepareCheckedI = _this.prepareCheckedItems(val, type),
            values = _this$prepareCheckedI.values,
            value = _this$prepareCheckedI.value;

        _this.setState({ checked: checked, values: values, value: value }, function () {
          _this.props.onChange(value, name, e);
        });
      }
    };

    _this.handleChange = function (e) {
      var _this$props3 = _this.props,
          type = _this$props3.type,
          name = _this$props3.name;

      var el = e.currentTarget;
      var value = el.value;


      _this.prepareValues(value, name, type, e);
    };

    _this.markInput = function (value, type, checked) {
      if (type === 'checkbox') {
        return checked[value] ? checked[value].status : false;
      }

      return _this.state.value === value;
    };

    _this.thisInput = _react2.default.createRef();
    return _this;
  }

  _createClass(InputGroup, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
      this.buildChecked();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (prevProps.classList && this.props.classList) {
        if (prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
          this.prepareClassList();
        }
      }

      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        if (!this.compareArrays(prevProps.value, this.props.value)) {
          this.buildChecked();
        }
      }
      // if (prevProps.hasOwnProperty('options') && this.props.hasOwnProperty('options')) {
      //   if (!this.compareArrays(prevProps.options, this.props.options)) {
      //     this.buildChecked();
      //   }
      // }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          type = _props.type,
          name = _props.name,
          options = _props.options,
          ref = _props.ref;
      var _state = this.state,
          classList = _state.classList,
          checked = _state.checked;

      var reference = ref || this.thisInput;

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(
          'div',
          { className: classList },
          options.map(function (opt) {
            return _react2.default.createElement(
              'div',
              { className: 'radio-group', key: opt.value },
              _react2.default.createElement(
                _Label2.default,
                { htmlFor: type + '-' + name + '-' + opt.value },
                _react2.default.createElement('input', _extends({ ref: reference,
                  type: type,
                  name: name,
                  id: type + '-' + name + '-' + opt.value,
                  className: 'input',
                  onChange: _this2.handleChange,
                  checked: _this2.markInput(opt.value, type, checked)
                }, opt.attrs, {
                  value: opt.value })),
                opt.label
              )
            );
          })
        )
      );
    }
  }]);

  return InputGroup;
}(_react.Component);

InputGroup.propTypes = {
  type: _propTypes2.default.oneOf(['checkbox', 'radio']).isRequired,
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  options: _propTypes2.default.array.isRequired,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  value: _propTypes2.default.array,
  ref: _propTypes2.default.object
};

InputGroup.defaultProps = {
  value: []
};

exports.default = InputGroup;