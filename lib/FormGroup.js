'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

var _Feedback = require('./Feedback');

var _Feedback2 = _interopRequireDefault(_Feedback);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _Label = require('./Label');

var _Label2 = _interopRequireDefault(_Label);

var _Input = require('./Input');

var _Input2 = _interopRequireDefault(_Input);

var _InputGroup = require('./InputGroup');

var _InputGroup2 = _interopRequireDefault(_InputGroup);

var _Select = require('./Select');

var _Select2 = _interopRequireDefault(_Select);

var _Autocomplete = require('./Autocomplete');

var _Autocomplete2 = _interopRequireDefault(_Autocomplete);

var _InputMask = require('./InputMask');

var _InputMask2 = _interopRequireDefault(_InputMask);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Components


var FormGroup = function (_Component) {
  _inherits(FormGroup, _Component);

  function FormGroup() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, FormGroup);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = FormGroup.__proto__ || Object.getPrototypeOf(FormGroup)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      boxClassList: ''
    }, _this.prepareClassList = function () {
      var boxClassList = _this.props.boxClassList;


      if (boxClassList.length <= 0) {
        boxClassList.push('full-width');
      }
      var boxClassListString = _utils2.default.createClassList(boxClassList);

      _this.setState({ boxClassListString: boxClassListString });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(FormGroup, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (prevProps.boxClassList.length < this.props.boxClassList.length || _utils2.default.checkClassListNames(prevProps.boxClassList, this.props.boxClassList)) {
        this.prepareClassList();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          type = _props.type,
          name = _props.name,
          attrs = _props.attrs,
          feedback = _props.feedback,
          icon = _props.icon,
          label = _props.label,
          classList = _props.classList,
          onChange = _props.onChange,
          isValid = _props.isValid,
          isInvalid = _props.isInvalid,
          defaultText = _props.defaultText,
          options = _props.options,
          dataList = _props.dataList,
          boxClassList = _props.boxClassList,
          optionTemplate = _props.optionTemplate,
          value = _props.value,
          mask = _props.mask,
          noDefault = _props.noDefault;
      var boxClassListString = this.state.boxClassListString;


      return _react2.default.createElement(
        'div',
        { className: boxClassListString },
        _react2.default.createElement(
          'div',
          { className: 'form-group' },
          label && _react2.default.createElement(
            _Label2.default,
            { htmlFor: name },
            label
          ),
          _react2.default.createElement(
            'div',
            { className: 'input-wrap' },
            function () {
              switch (type) {
                case 'textarea':
                  return _react2.default.createElement(_Input2.default, { type: type,
                    name: name,
                    classList: classList,
                    onChange: onChange,
                    attrs: attrs,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    value: value
                  });
                case 'radio':
                case 'checkbox':
                  return _react2.default.createElement(_InputGroup2.default, { type: type,
                    name: name,
                    onChange: onChange,
                    classList: classList,
                    options: options,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    value: value
                  });
                case 'select':
                  return _react2.default.createElement(_Select2.default, { name: name,
                    classList: classList,
                    onChange: onChange,
                    attrs: attrs,
                    options: options,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    defaultText: defaultText,
                    value: value,
                    noDefault: noDefault
                  });
                case 'autocomplete':
                  return _react2.default.createElement(_Autocomplete2.default, { name: name,
                    classList: classList,
                    onChange: onChange,
                    attrs: attrs,
                    dataList: dataList,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    boxClassList: boxClassList,
                    optionTemplate: optionTemplate,
                    value: value
                  });
                case 'masked':
                  return _react2.default.createElement(_InputMask2.default, { name: name,
                    classList: classList,
                    onChange: onChange,
                    attrs: attrs,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    mask: mask,
                    value: value
                  });
                default:
                  return _react2.default.createElement(_Input2.default, { type: type,
                    name: name,
                    classList: classList,
                    onChange: onChange,
                    attrs: attrs,
                    isValid: isValid,
                    isInvalid: isInvalid,
                    value: value
                  });
              }
            }(),
            icon && _react2.default.createElement(_Icon2.default, { classList: icon }),
            feedback && _react2.default.createElement(
              _Feedback2.default,
              null,
              feedback
            )
          )
        )
      );
    }
  }]);

  return FormGroup;
}(_react.Component);

FormGroup.propTypes = {
  type: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array,
  onChange: _propTypes2.default.func.isRequired,
  attrs: _propTypes2.default.object,
  feedback: _propTypes2.default.node,
  icon: _propTypes2.default.array,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  label: _propTypes2.default.node,
  defaultText: _propTypes2.default.string,
  options: _propTypes2.default.array,
  dataList: _propTypes2.default.array,
  boxClassList: _propTypes2.default.array,
  optionTemplate: _propTypes2.default.node,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.array, _propTypes2.default.number]),
  mask: _propTypes2.default.string,
  noDefault: _propTypes2.default.bool
};

FormGroup.defaultProps = {
  defaultText: 'Select an option...',
  options: [],
  dataList: [],
  classList: [],
  boxClassList: []
};

exports.default = FormGroup;