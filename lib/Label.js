'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Label = function (_Component) {
  _inherits(Label, _Component);

  function Label() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Label);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Label.__proto__ || Object.getPrototypeOf(Label)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      classList: ''
    }, _this.prepareClassList = function () {
      var classList = _this.props.classList;

      var classListString = _utils2.default.createClassList(classList);

      _this.setState({ classList: classListString });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Label, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (prevProps.hasOwnProperty('classList') && this.props.hasOwnProperty('classList')) {
        if (prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
          this.prepareClassList();
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          htmlFor = _props.htmlFor,
          children = _props.children;
      var classList = this.state.classList;


      return _react2.default.createElement(
        'label',
        { htmlFor: htmlFor, className: classList + ' control-label' },
        children
      );
    }
  }]);

  return Label;
}(_react.Component);

Label.propTypes = {
  htmlFor: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array,
  children: _propTypes2.default.any
};

exports.default = Label;