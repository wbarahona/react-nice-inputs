import React, { Component, Fragment } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Input extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
  }

  state = {
    classList: '',
    value: ''
  }

  componentDidMount() {
    this.prepareClassList();
    this.prepopulateInput();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.classList && this.props.classList && prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
      this.prepareClassList();
    }

    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if (prevProps.value !== this.props.value) {
        this.prepopulateInput();
      }
    }
  }

  prepopulateInput = () => {
    const { value } = { ... this.props };

    if (value) {
      this.setState({ value });
    } else {
      this.setState({ value: '' });
    }
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid } = this.props;
    let classListString = utils.createClassList(classList, isValid, isInvalid);

    classListString = `input ${ classListString }`;

    this.setState({ classList: classListString });
  }

  handleChange = e => {
    const { name } = this.props;
    const value = e.target.value;

    this.setState({ value });
    this.props.onChange(value, name, e);
  }

  render() {
    const { type, name, attrs} = this.props;
    const { classList, value } = this.state;

    return (
      <Fragment>
        {(() => {
          switch(type) {
            case 'textarea':
              return <textarea ref={ this.thisInput } name={ name } id={ name } className={ classList } onChange={ this.handleChange } value={ value } { ...attrs } />;
            default:
              return <input ref={ this.thisInput } type={ type } name={ name } id={ name } className={ classList } onChange={ this.handleChange } value={ value } { ...attrs } />;
          }
        })()}
      </Fragment>
    );
  }
}

Input.propTypes = {
  type: propTypes.string.isRequired,
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  attrs: propTypes.object,
  onChange: propTypes.func.isRequired,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  value: propTypes.string
};

export default Input;
