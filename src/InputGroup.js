import React, { Component, Fragment } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

// Components
import Label from './Label';

class InputGroup extends Component {
  constructor(props) {
    super(props);
    // create a ref to store the thisInput DOM element
    this.thisInput = React.createRef();
  }

  state = {
    classList: '',
    checked: {},
    values: [],
    value: ''
  }

  componentDidMount() {
    this.prepareClassList();
    this.buildChecked();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.classList && this.props.classList) {
      if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }
    }

    if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
      if (!this.compareArrays(prevProps.value, this.props.value)) {
        this.buildChecked();
      }
    }
    // if (prevProps.hasOwnProperty('options') && this.props.hasOwnProperty('options')) {
    //   if (!this.compareArrays(prevProps.options, this.props.options)) {
    //     this.buildChecked();
    //   }
    // }
  }

  compareArrays = (prevValue, currentValue) => {
    return prevValue.length === currentValue.length && prevValue.sort().every((value, index) => { return value === currentValue.sort()[index];});
  }

  resetInputs = () => {
    const checked = [];

    this.props.options.map(opt => {
      // const chk = { status: value.includes(opt.value) };
      const chk = { status: false };

      checked[opt.value] =  { ... chk };
    });
    this.setState({ values: [], value: '', checked });
  }

  buildChecked = () => {
    const { options, value, type } = this.props;
    const checked = {};
    const values = [];
    let valueString = '';

    // set all options to unchecked
    options.map(opt => {
      // const chk = { status: value.includes(opt.value) };
      const chk = { status: false };

      checked[opt.value] =  { ... chk };

      if (value.includes(opt.value)) {
        values.push(opt.value);
      }
    });

    valueString = values.join(',');

    for (let i = 0; i < value.length; i++) {
      const val = value[i];

      if (checked[val]) {
        checked[val].status = true;
      }
      if (type === 'radio') {
        valueString = values[0];
        if (values.length >= 2 ) { console.warn('react-nice-inputs: we detected you are using "radio" buttons and sending the value props with more than one element, using the last element from value array as default'); }

        break;
      }
    }

    this.setState({ checked, values, value: valueString });
  }

  prepareClassList = () => {
    const { classList, isValid, isInvalid } = this.props;
    const classListCopy = [ ... classList, 'input-list-wrap'];
    const wrapClassListString = utils.createClassList(classListCopy, isValid, isInvalid);

    this.setState({ classList: wrapClassListString });
  }

  prepareCheckedItems = (value, type) => {
    const stateCopy = { ... this.state };
    const { checked } = stateCopy;
    let values = [];

    if (type === 'checkbox') {
      for (const item in checked) {
        if (checked.hasOwnProperty(item)) {
          const el = checked[item];

          if (el.status) {
            values.push(item);
          }
        }
      }
    } else {
      values = [ value ];
    }

    return {
      values,
      value: values.join(',')
    };
  }

  prepareValues = (val, name, type, e) => {
    const stateCopy = { ... this.state };
    const { checked } = stateCopy;
    const checkedLen = Object.keys(checked).length;

    if (checkedLen > 0) {
      checked[val].status = !checked[val].status;

      const { values, value } = this.prepareCheckedItems(val, type);

      this.setState({ checked, values, value }, () => {
        this.props.onChange(value, name, e);
      });
    }
  }

  handleChange = e => {
    const { type, name } = this.props;
    const el = e.currentTarget;
    const { value } = el;

    this.prepareValues(value, name, type, e);
  }

  markInput = (value, type, checked) => {
    if (type === 'checkbox') {
      return (checked[value]) ? checked[value].status : false;
    }

    return this.state.value === value;
  }

  render() {
    const { type, name, options, ref } = this.props;
    const { classList, checked } = this.state;
    const reference = ref || this.thisInput;

    return (
      <Fragment>
        <div className={ classList }>
          {
            options.map((opt) => (
              <div className="radio-group" key={ opt.value }>
                <Label htmlFor={ `${ type }-${ name }-${ opt.value }` }>
                  <input ref={ reference }
                    type={ type }
                    name={ name }
                    id={ `${ type }-${ name }-${ opt.value }` }
                    className="input"
                    onChange={ this.handleChange }
                    checked={ this.markInput(opt.value, type, checked) }
                    { ...opt.attrs }
                    value={ opt.value } />
                  { opt.label }
                </Label>
              </div>
            ))
          }
        </div>
      </Fragment>
    );
  }
}

InputGroup.propTypes = {
  type: propTypes.oneOf(['checkbox', 'radio']).isRequired,
  name: propTypes.string.isRequired,
  classList: propTypes.array.isRequired,
  onChange: propTypes.func.isRequired,
  options: propTypes.array.isRequired,
  isValid: propTypes.bool,
  isInvalid: propTypes.bool,
  value: propTypes.array,
  ref: propTypes.object
};

InputGroup.defaultProps = {
  value: []
};

export default InputGroup;
