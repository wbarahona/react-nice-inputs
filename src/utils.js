const utils = {};

utils.debounce = (fn, delay) => {
  let timer = null;

  return function ret() {
    const self = this;
    const args = arguments;

    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(self, args);
    }, delay);
  };
};

utils.createClassList = (classListProp, isValid, isInvalid) => {
  let classList = '';

  if (classListProp && classListProp.length > 0) {
    const classListCopy = [ ...classListProp ];

    if (isValid) {
      classListCopy.push('is-valid');
      const idx = classListCopy.indexOf('is-invalid');

      classListCopy.slice(idx, 1);
    } else {
      const idx = classListCopy.indexOf('is-valid');

      classListCopy.slice(idx, 1);
    }

    if (isInvalid) {
      classListCopy.push('is-invalid');
      const idx = classListCopy.indexOf('is-valid');

      classListCopy.slice(idx, 1);
    } else {
      const idx = classListCopy.indexOf('is-invalid');

      classListCopy.slice(idx, 1);
    }

    classList = classListCopy.join(' ', ',');
  }

  return classList;
};

utils.checkClassListNames = (prevClassList, currClassList) => {
  for (let i = 0; i < prevClassList.length; i++) {
    const prevClassListName = prevClassList[i];
    const currClassListName = currClassList[i];

    if ( prevClassListName !== currClassListName ) {
      return true;
    }
  }

  return false;
};

utils.findInOptions = (options, value) => {
  return options.find(e => e.value === value);
};

export default utils;
